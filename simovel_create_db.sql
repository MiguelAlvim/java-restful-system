CREATE DATABASE Simovel;

USE DATABASE Simovel;

CREATE TABLE Simulation(
	id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	creator_name text DEFAULT "",
	property_description text DEFAULT "",
	installments integer DEFAULT 0,
	property_price DECIMAL(11,2) NOT NULL,
	interest_rate DECIMAL(11,2) NOT NULL
);