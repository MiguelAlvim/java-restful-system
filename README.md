# Basic Information #

* WebService created with NetBeabs 8.2
* Utilizes JAV-RX with Jersey (pretty default NetBeans settings)
* Tested on GlassFish 5.01 (Java EE 8)
* Uses the following external Libs
	* Uses JDBC for DB access [mysql-connector-java-5.1.23-bin.jar](https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java-5.1.23.zip)
* The Rest services are in /Simovel/<service name and params here>
	
# What the Application Does #

* It allows for it's user to input a property price and interest rates for a instalment price simulation
	* The user can save it's simulation in a mySQL databse and access it again later through the application