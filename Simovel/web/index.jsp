<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Sim�vel</title>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">  
        <script src="https://kit.fontawesome.com/26ef85518d.js" crossorigin="anonymous"></script>
    </head>
    <body onLoad="loadSelect();loadSimulation();loadInfo()">
            <div class="page-header pb-2 mt-2 border-bottom bg-light">
                <h1 class='text-center'>Sim�vel - Property Financing Simulator</h1>
            </div>
        <div style='margin-left: 20em; margin-top: 2em; margin-right: 20em' class="p-3 mb-2 bg-secondary border border-secondary">
            <form action="http://localhost:8080/Simovel/insert" method="post" id="form">   
                <div class='row justify-content-center'>
                    <div class='form-group col'>                
                        <label>Property Price:</label><br>        
                        <input type="number" class="form-control no_spinner" id="property_price" name="property_price" step="0.01"><br>  
                    </div>

                    <div class='form-group col'>                
                        <label>Interest Rate (%):</label><br>        
                        <input type="number" class="form-control no_spinner" id="interest_rate" name="interest_rate" step="0.01"><br>     
                    </div>      

                    <div class='form-group col'>                
                        <label>Installments:</label><br>        
                        <input type="number" class="form-control no_spinner" id="installments" name="installments"><br>    
                    </div>      
                </div>

  
                <div class='row justify-content-center'>
                    <div class='col'>                
                        <label>Property Description:</label><br>        
                        <input type="text" class="form-control" id="property_description" name="property_description"><br>
                    </div>      

                    <div class='col'>                
                        <label>Creator Name:</label><br>        
                        <input type="text" class="form-control" id="creator_name" name="creator_name"><br>
                    </div>  
                </div>    

                <input type="hidden" id="id" name="id">
                <input type="submit" id="submit"value="Save Simulation" class='btn btn-primary'>
            </form>
            <div class='row border-top p-1 border-light' style='margin-top: 1em'>
                    <div class='col-2' style='margin-top: 1.4em'>                
                        <button value="Calculate Costs" onClick="calculateInterest()" class='btn btn-success'>Calculate Costs</button>
                    </div>
                    <div class='col-3'>                
                        <label>Final Cost</label>
                        <input type="text" id="cost_final" name="cost" class='input-group-text'readonly>
                    </div>
                    <div class='col'>                
                        <label>Per Installment Cost</label>
                        <input type="text" id="cost_perInstallment" name="cost" class='input-group-text' readonly>
                    </div>
            </div>
        </div>
        
        <footer class="fixed-bottom border-top p-1 bg-light">
            <div class='row'>
                <div class='col-1' style='margin-left: 1em'>
                    <div  style='margin-left:0.6em'>
                        <i class="far fa-building fa-3x"></i>
                    </div>
                    <h6 style='font-weight: bold'>Sim�vel</h6>
                    <small class="d-block mb-3 text-muted" style='margin-left:-1.4em'>&copy;<span id="copy_right_msg"></span></small>
                </div>
                <div class='col-2'>
                    <div>            
                        <label>Access Saved Simulations</label><br>
                        <select id="select_saved_simulations" onchange="viewSimulation()" class="form-control">
                            <option value="-1" selected disabled>Choose</option>
                        </select>
                    </div>
                </div>
            </div>
        </footer>
    </body>
    <script>
        //Global variables
        var updating = false;
        
        //Functions
        function loadInfo(){
            let cr = document.getElementById('copy_right_msg').innerHTML = " 2020 - "+(new Date().getFullYear()).toString();
        }
        function loadSelect(){
            let select = document.getElementById("select_saved_simulations")      
            for(let i = select.length; i >= 1; --i) {
               select.remove(i);
            }          
            $.ajax({
                url: 'http://localhost:8080/Simovel/list/',
                type: 'GET',
                success: function(data){
                    let response = JSON.parse(data)
                    if(response['success']){
                        for(let key of Object.keys(response['result'])){                            
                            let opt = document.createElement('option');
                            opt.value = response['result'][key]['id'];
                            opt.innerHTML = response['result'][key]['id']+": "+response['result'][key]['creator_name']+" - "+response['result'][key]['property_description'];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function(data){
                    alert("Error on retrieving data from Server\n"+datadata.toString())
                    console.log(data);
                }
            });
        }
        
        function viewSimulation(){
            let select = document.getElementById("select_saved_simulations")
            let url = window.location.href+select.value
            let re  = new RegExp('.*\/')
            let ans = re.exec(url)
            if(ans!==null){
                url = ans[0]+select.value
            }
            document.location.href = url
        }
        
        function loadSimulation(){
            let url = window.location.href;
            let re  = new RegExp('\/\\d+')
            let ans = re.exec(url)
            if(ans!==null){
                let id = ans[0].substr(1)
                updating = true
                document.getElementById("form").action = "http://localhost:8080/Simovel/update"
                document.getElementById("submit").value = "Update Simulation"
                $.ajax({
                    url: 'http://localhost:8080/Simovel/get/',
                    type: 'POST',
                    data: 'id='+id,
                    success: function(data){
                        let response = JSON.parse(data)
                        if(response['success']){
                           document.getElementById("property_price").value       = parseFloat(response['result'][0]['property_price'])
                           document.getElementById("interest_rate").value        = parseFloat(response['result'][0]['interest_rate'])
                           document.getElementById("installments").value         = parseInt(response['result'][0]['installments'])
                           document.getElementById("property_description").value = response['result'][0]['property_description']
                           document.getElementById("creator_name").value         = response['result'][0]['creator_name']
                           document.getElementById("id").value                   = parseInt(response['result'][0]['id'])
                           calculateInterest()
//                           document.getElementById("select_saved_simulations").value = document.getElementById("id").value
//                           document.getElementById("select_saved_simulations").selectedIndex = document.getElementById("id").value
                        }else{
                            alert("ID given does not exists")
                        }
                    },
                    error: function(data){
                        alert("Error on retrieving data from Server\n"+datadata.toString())
                        console.log(data);
                    }
                });
            }        
        }
        function calculateInterest(){
            let price = parseFloat(document.getElementById("property_price").value);
            let interest = parseFloat(document.getElementById("interest_rate").value);
            let installments = parseInt(document.getElementById("installments").value);
            
            if(installments > 1){
                let inter = ((price/installments)*(1+interest/100)).toFixed(2)
                let total = ((price/installments)*(1+interest/100)*installments).toFixed(2)
                
                if(Number.isNaN(inter)){
                    document.getElementById("cost_perInstallment").value = "Invalid entry Givenn"
                }else{
                    document.getElementById("cost_perInstallment").value = inter
                }
                
                if(Number.isNaN(total)){
                    document.getElementById("cost_final").value = "Invalid entry Givenn"
                }else{
                    document.getElementById("cost_final").value = total
                }
            }else{
                if(Number.isNaN(price)){
                    document.getElementById("cost_final").value = "Invalid entry Given"
                    document.getElementById("cost_perInstallment").value = "Invalid entry Givenn"
                }else{
                    document.getElementById("cost_final").value = parseFloat(price).toFixed(2)
                    document.getElementById("cost_perInstallment").value = parseFloat(price).toFixed(2)
                }
            }
        }
        
        //Form functionality
        $(document).ready(function() { 
            $('#form').submit(function(event){
                event.preventDefault();
                let post_url = $(this).attr("action");
                let request_method = $(this).attr("method");
                let form_data = $(this).serialize();
                $.ajax({
                    url : post_url,
                    type: request_method,
                    data : form_data,
                    success: function(data){
                        console.log(data)
                        let response = JSON.parse(data)
                        if(updating){
                            if(response['success'])
                                alert("Simulation updated");
                            else
                                alert("Error on updating data on Server\n"+response.toString())
                        }else{
                            if(response['success']){
                                let url = window.location.href+response['result']['id']
                                let re  = new RegExp('.*\/')
                                let ans = re.exec(url)
                                if(ans!==null){
                                    url = ans[0]+response['result']['id']
                                }
                                alert("Simulation saved with ID = "+response['result']['id']+"\nYou can access it from the selection on the bottom of the page or with the URL '"+url+"'");
                                document.location.href = url
                            }else{
                                alert("Error on inserting data on Server\n"+response.toString())
                            }
                        }
                    },
                    error: function(data){
                        if(updating)
                            alert("Error on updating data on Server\n"+datadata.toString())
                        else
                            alert("Error on inserting data on Server\n"+datadata.toString())
                    }
                });
            });
        });
        
    </script>
</html>
