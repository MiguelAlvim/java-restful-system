package rest;

import database.DBController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;

/**
 *
 * @author Miguel Alvim
 */
@Path("")
public class ResourceSimulation {   
    @Context
    private ServletContext servletContext;
    
    private String baseName     = "simovel";
    private String basePort     = "3306";
    private String serverName   = "localhost";
    private String user         = "root";
    private String userpassword = "";
    
    @GET
    @Produces("text/html; charset=UTF-8")
    public String indexPage() {   
        String result = "",readLine;
        InputStream index = servletContext.getResourceAsStream("/index.jsp");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(index, "ISO-8859-1"));
            while((readLine = reader.readLine()) != null){
                result += readLine+"\n";
            }
        } catch (IOException ex) {
            return ex.toString();
        } 
        return result;
//        return "Nothing four you to see here. Go away now";    
//        try {
//            return ""+servletContext.getResource("/imgs/real-state.svg");
//        } catch (MalformedURLException ex) {
//            
//            return "";
//        }
    }   
    
    @Path("list")
    @GET
    @Produces("text/plain; charset=UTF-8")
    public String list() {
        Connection con = null;
        String content="{";
        try {
            con = DBController.establishConnection(serverName, basePort, baseName, user, userpassword);
            ResultSet result = DBController.executeSelect(con,"SELECT * FROM Simulation",null);
            content+="\"success\":true,\"result\":{";
            int counter = 0;
            while(result.next()){
                content +="\""+counter+"\":{";
                content += "\"id\":"+result.getInt("id");
                content += ",\"creator_name\":\""+result.getString("creator_name")+"\"";
                content += ",\"property_description\":\""+result.getString("property_description")+"\"";
                content += ",\"installments\":\""+result.getInt("installments")+"\"";
                content += ",\"property_price\":\""+result.getInt("property_price")+"\"";
                content += ",\"interest_rate\":\""+result.getDouble("interest_rate")+"\"";
                if(!result.isLast())
                    content +="},";
                else
                    content +="}}";
                ++counter;
                    
            }
            con.close();
            content+="}";
        } catch (ClassNotFoundException | SQLException ex) {
            content+="\"success\":false,";
            content+="\"error\":\""+ ex.toString()+"\"";
        }
        return content;
    }
    
    @Path("/{id}")
    @GET
    @Produces("text/html; charset=UTF-8")
    public String getView(@PathParam("id") int id) {
        return indexPage();
    }
        
    @Path("get")
    @POST
    @Produces("text/plain; charset=UTF-8")
    public String get(@FormParam("id") int id) {
        Connection con = null;
        String content="{";
        try {
            con = DBController.establishConnection(serverName, basePort, baseName, user, userpassword);
            ResultSet result = DBController.executeSelect(con,"SELECT * FROM Simulation WHERE id = ?",new String[]{String.valueOf(id)});
            content+="\"success\":true,\"result\":{";
            int counter = 0;
            while(result.next()){
                content +="\""+counter+"\":{";
                content += "\"id\":"+result.getInt("id");
                content += ",\"creator_name\":\""+result.getString("creator_name")+"\"";
                content += ",\"property_description\":\""+result.getString("property_description")+"\"";
                content += ",\"installments\":\""+result.getInt("installments")+"\"";
                content += ",\"property_price\":\""+result.getInt("property_price")+"\"";
                content += ",\"interest_rate\":\""+result.getDouble("interest_rate")+"\"";
                if(!result.isLast())
                    content +="},";
                else
                    content +="}}";
                ++counter;
                    
            }
            con.close();
            content+="}";
        } catch (ClassNotFoundException | SQLException ex) {
            content+="\"success\":false,";
            content+="\"error\":\""+ ex.toString()+"\"";
        }
        return content;
    }
    
    @Path("insert")
    @POST
    @Produces("text/plain; charset=UTF-8")
    public String insert(@FormParam("creator_name") String creator_name,
                         @FormParam("property_description") String property_description,
                         @FormParam("installments") String installments,
                         @FormParam("property_price") String proprety_price,
                         @FormParam("interest_rate") String interest_rate) {
        Connection con = null;
        String content="{";
        try {
            con = DBController.establishConnection(serverName, basePort, baseName, user, userpassword);
            long result = DBController.executeInsert(
                    con,
                    "INSERT INTO Simulation(creator_name, property_description, installments, property_price, interest_rate) VALUES (?,?,?,?,?)",
                    new String[]{
                        creator_name,
                        property_description,
                        installments,
                        proprety_price,
                        interest_rate
                    }
            );
            content+="\"success\":true,\"result\":{\"id\":"+result+"}}";
            
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            content+="\"success\":false,";
            content+="\"error\":\""+ ex.toString()+"\"";
        }
        return content;
    }
    
    @Path("update")
    @POST
    @Produces("text/plain; charset=UTF-8")
    public String update(@FormParam("creator_name") String creator_name,
                         @FormParam("property_description") String property_description,
                         @FormParam("installments") String installments,
                         @FormParam("property_price") String proprety_price,
                         @FormParam("interest_rate") String interest_rate,
                         @FormParam("id") String id) {
        Connection con = null;
        String content="{";
        String[] params = new String[]{creator_name,property_description,installments,proprety_price,interest_rate,id};
        try {
            con = DBController.establishConnection(serverName, basePort, baseName, user, userpassword);
            ResultSet result = DBController.executeUpdate(
                    con,
                    "UPDATE simulation SET creator_name = ?, property_description = ?, installments = ?, property_price = ?, interest_rate = ? WHERE id = ?",
                    params
            );
            content+="\"success\":true,\"result\":{\"rows\":{";
            int counter = 0;
            while(result.next()){
                content+="\""+counter+"\":\""+result.getInt(1)+"\"";
                if(!result.isLast())
                    content+=",";
                ++counter;
            }
            content+="}}}";
            
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            content+="\"success\":false,";
            content+="\"params\":\""+Arrays.toString(params)+"\",";
            content+="\"error\":\""+ ex.toString()+"\"";
        }
        return content;
    }
    
}