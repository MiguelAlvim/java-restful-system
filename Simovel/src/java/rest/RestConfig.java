package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Miguel Alvim
 */
@ApplicationPath("")
public class RestConfig extends Application{
    
}
