package database;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
/**
 * Has all the necessary functions to connect and access a mySQL database
 * 
 * @author Miguel Alvim
 */
public class DBController {
    @Context
    static private ServletContext context;
    
    /**
     * Connects using the default parameters
     * 
     * @return a connection to a database or null in case of an error
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static Connection establishTestConnection() throws ClassNotFoundException, SQLException{ 
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
        return con;
    }
    
    /**
     * Connects using parameters given by the user
     * 
     * @param database The database name
     * @param servername The server name (ex: 'localhost')
     * @param serverport The server port where the mySQL is (ex: '3306')
     * @param user The user to which we will use to make the connection
     * @param password The user password
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static Connection establishConnection(String servername, String serverport, String database, String user, String password) throws ClassNotFoundException, SQLException{ 
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection con = DriverManager.getConnection("jdbc:mysql://"+servername+":"+serverport+"/"+database, user, password);
        return con;
    }
    /**
     * Executes and returns the results in a array of a select query that is sent in a prepared statement format
     * 
     * @param con Connection type variable with the DB connection
     * @param query The query to be executed in String form
     * @param vals The values of the query in a string array format
     * @return an Array with all the answers
     * @throws SQLException 
     */
    public static ResultSet executeSelect(Connection con, String query, String[] vals) throws SQLException{        
        PreparedStatement stmnt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);        
        if(vals != null)
            for(int i=0;i<vals.length;++i){
                stmnt.setString(i+1, vals[i]);
            }
        ResultSet results = stmnt.executeQuery();
        return results;            
    }
    
    /**
     * Executes and returns the new id from the created row from an insert query that is sent in a prepared statement format
     * 
     * @param con Connection type variable with the DB connection
     * @param query The query to be executed in String form
     * @param vals The values of the query in a string array format
     * @return an Array with all the answers
     * @throws SQLException 
     */
    public static long executeInsert(Connection con, String query, String[] vals) throws SQLException{        
        PreparedStatement stmnt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
        if(vals != null)
            for(int i=0;i<vals.length;++i){
                stmnt.setString(i+1, vals[i]);
            }
        stmnt.executeUpdate();
        ResultSet result = stmnt.getGeneratedKeys();
        result.next();
        return result.getLong(1);
    }
    
    
    /**
     * Executes and returns the affected rows in a array of an update query that is sent in a prepared statement format
     * It's pretty similar to executeInsert and can actually also run insert queries but do not automatically returns the new id form the query
     * If you run: " ResultSet result = DBController.executeUpdate(con,query,vals);result.next();int rowId = result.getLong(1) ", rowId will have the same result from executeInsert
     * 
     * @param con Connection type variable with the DB connection
     * @param query The query to be executed in String form
     * @param vals The values of the query in a string array format
     * @return an Array with all the answers
     * @throws SQLException 
     */
    public static ResultSet executeUpdate(Connection con, String query, String[] vals) throws SQLException{        
        PreparedStatement stmnt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
        if(vals != null)
            for(int i=0;i<vals.length;++i){
                stmnt.setString(i+1, vals[i]);
            }
        stmnt.executeUpdate();
        return stmnt.getGeneratedKeys();
    }
}
